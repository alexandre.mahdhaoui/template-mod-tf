
resource "aws_organizations_account" "account" {
  close_on_deletion = var.close_on_deletion
  email = var.email
  name = var.name
  lifecycle {
    ignore_changes = [role_name]
  }
}
variable "close_on_deletion" {
  description = "If true, a deletion event will close the account. Otherwise, it will only remove from the organization. This is not supported for GovCloud accounts."
  type        = bool
}
variable "email" {
  description = "Email address of the owner to assign to the new member account. This email address must not already be associated with another AWS account."
  type        = string
}
variable "name" {
  description = "Friendly name for the member account."
  type        = string
}
variable "provider_aws_region" {
  description = "aws region for the provider."
  type        = string
}
variable "provider_aws_role_arn" {
  description = "role arn the aws provider will assume."
  type        = string
}
variable "tag_kind_name" {
  description = "Tag should comply to https://gitlab.com/alexandre.mahdhaoui/spec-tag"
  type        = string
}
variable "tag_kind_version" {
  description = "Tag should comply to https://gitlab.com/alexandre.mahdhaoui/spec-tag"
  type        = string
}
variable "tag_instance_id" {
  description = "Tag should comply to https://gitlab.com/alexandre.mahdhaoui/spec-tag"
  type        = string
}
variable "tag_instance_name" {
  description = "Tag should comply to https://gitlab.com/alexandre.mahdhaoui/spec-tag"
  type        = string
}
variable "tag_instance_version" {
  description = "Tag should comply to https://gitlab.com/alexandre.mahdhaoui/spec-tag"
  type        = string
}
variable "tag_resource_domain" {
  description = "Tag should comply to https://gitlab.com/alexandre.mahdhaoui/spec-tag"
  type        = string
}
variable "tag_resource_id" {
  description = "Tag should comply to https://gitlab.com/alexandre.mahdhaoui/spec-tag"
  type        = string
}
variable "tag_resource_name" {
  description = "Tag should comply to https://gitlab.com/alexandre.mahdhaoui/spec-tag"
  type        = string
}
variable "tag_resource_namespace" {
  description = "Tag should comply to https://gitlab.com/alexandre.mahdhaoui/spec-tag"
  type        = string
}
variable "tag_resource_region" {
  description = "Tag should comply to https://gitlab.com/alexandre.mahdhaoui/spec-tag"
  type        = string
}
variable "tag_resource_role" {
  description = "Tag should comply to https://gitlab.com/alexandre.mahdhaoui/spec-tag"
  type        = string
}
variable "tag_resource_stage" {
  description = "Tag should comply to https://gitlab.com/alexandre.mahdhaoui/spec-tag"
  type        = string
}
variable "tag_resource_created_by_domain" {
  description = "Tag should comply to https://gitlab.com/alexandre.mahdhaoui/spec-tag"
  type        = string
}
variable "tag_resource_created_by_service" {
  description = "Tag should comply to https://gitlab.com/alexandre.mahdhaoui/spec-tag"
  type        = string
}
variable "tag_service_name" {
  description = "Tag should comply to https://gitlab.com/alexandre.mahdhaoui/spec-tag"
  type        = string
}
variable "tag_service_version" {
  description = "Tag should comply to https://gitlab.com/alexandre.mahdhaoui/spec-tag"
  type        = string
}
variable "tag_service_parent_name" {
  description = "Tag should comply to https://gitlab.com/alexandre.mahdhaoui/spec-tag"
  type        = string
}
variable "tag_service_parent_version" {
  description = "Tag should comply to https://gitlab.com/alexandre.mahdhaoui/spec-tag"
  type        = string
}
variable "tag_business_owner" {
  description = "Tag should comply to https://gitlab.com/alexandre.mahdhaoui/spec-tag"
  type        = string
}
variable "tag_business_project" {
  description = "Tag should comply to https://gitlab.com/alexandre.mahdhaoui/spec-tag"
  type        = string
}
variable "tag_business_sla" {
  description = "Tag should comply to https://gitlab.com/alexandre.mahdhaoui/spec-tag"
  type        = string
}
variable "tag_business_tenant" {
  description = "Tag should comply to https://gitlab.com/alexandre.mahdhaoui/spec-tag"
  type        = string
}
variable "tag_business_unit" {
  description = "Tag should comply to https://gitlab.com/alexandre.mahdhaoui/spec-tag"
  type        = string
}
variable "tag_automation_date_time" {
  description = "Tag should comply to https://gitlab.com/alexandre.mahdhaoui/spec-tag"
  type        = string
}
variable "tag_automation_opt_in" {
  description = "Tag should comply to https://gitlab.com/alexandre.mahdhaoui/spec-tag"
  type        = string
}
variable "tag_automation_opt_out" {
  description = "Tag should comply to https://gitlab.com/alexandre.mahdhaoui/spec-tag"
  type        = string
}
variable "tag_automation_security" {
  description = "Tag should comply to https://gitlab.com/alexandre.mahdhaoui/spec-tag"
  type        = string
}
variable "tag_mutex_author" {
  description = "Tag should comply to https://gitlab.com/alexandre.mahdhaoui/spec-tag"
  type        = string
}
variable "tag_mutex_locked" {
  description = "Tag should comply to https://gitlab.com/alexandre.mahdhaoui/spec-tag"
  type        = string
}
variable "tag_mutex_timestamp" {
  description = "Tag should comply to https://gitlab.com/alexandre.mahdhaoui/spec-tag"
  type        = string
}
variable "tag_security_compliance" {
  description = "Tag should comply to https://gitlab.com/alexandre.mahdhaoui/spec-tag"
  type        = string
}
variable "tag_security_confidentiality" {
  description = "Tag should comply to https://gitlab.com/alexandre.mahdhaoui/spec-tag"
  type        = string
}
terraform {
  backend "s3" {}
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= 4.38.0"
    }
  }
  required_version = ">= 1.3.4"
}
provider "aws" {
  region = var.provider_aws_region
  assume_role {
    role_arn = var.provider_aws_role_arn
  }
  default_tags {
    tags = {
      "kind/name" = var.tag_kind_name
      "kind/version" = var.tag_kind_version
      "instance/id" = var.tag_instance_id
      "instance/name" = var.tag_instance_name
      "instance/version" = var.tag_instance_version
      "resource/domain" = var.tag_resource_domain
      "resource/id" = var.tag_resource_id
      "resource/name" = var.tag_resource_name
      "resource/namespace" = var.tag_resource_namespace
      "resource/region" = var.tag_resource_region
      "resource/role" = var.tag_resource_role
      "resource/stage" = var.tag_resource_stage
      "resource/created-by/domain" = var.tag_resource_created_by_domain
      "resource/created-by/service" = var.tag_resource_created_by_service
      "service/name" = var.tag_service_name
      "service/version" = var.tag_service_version
      "service/parent/name" = var.tag_service_parent_name
      "service/parent/version" = var.tag_service_parent_version
      "business/owner" = var.tag_business_owner
      "business/project" = var.tag_business_project
      "business/sla" = var.tag_business_sla
      "business/tenant" = var.tag_business_tenant
      "business/unit" = var.tag_business_unit
      "automation/date-time" = var.tag_automation_date_time
      "automation/opt-in" = var.tag_automation_opt_in
      "automation/opt-out" = var.tag_automation_opt_out
      "automation/security" = var.tag_automation_security
      "mutex/author" = var.tag_mutex_author
      "mutex/locked" = var.tag_mutex_locked
      "mutex/timestamp" = var.tag_mutex_timestamp
      "security/compliance" = var.tag_security_compliance
      "security/confidentiality" = var.tag_security_confidentiality
      "kind/api"     = "organizations.aws.terraform"
      "kind/issuer"  = "alexandre.mahdhaoui.com"
      "kind/name"    = "TerraformResource"
      "kind/version" = "v0.1.0"
    }
  }
}
output "email" {
  description = "Email address of the owner to assign to the new member account. This email address must not already be associated with another AWS account."
  value = aws_organizations_account.account.email
}
output "name" {
  description = "Friendly name for the member account."
  value = aws_organizations_account.account.name
}
output "arn" {
  description = "ARN of the Account"
  value = aws_organizations_account.account.arn
}
output "tags_all" {
  description = "Key-value map of resource tags. If configured with a provider default_tags configuration block present, tags with matching keys will overwrite those defined at the provider-level."
  value = aws_organizations_account.account.tags_all
  sensitive = true
}